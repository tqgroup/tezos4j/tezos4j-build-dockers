#!/bin/sh

set -eu

VERSIONS="8 11 15"

DOCKER_REGISTRY_PATH="tqgroup/tezos4j/tezos4j-build-dockers"
CI_REGISTRY=${CI_REGISTRY:-registry.gitlab.com}

if [ -n "${CI_DEPLOY:-}" ]; then
  docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
fi

for v in $VERSIONS; do
  CANON_IMAGE_NAME="tezos4j-build-jdk$v"
  echo "FROM openjdk:$v-jdk-buster" > Dockerfile.jdk$v
  cat  Dockerfile.template >> Dockerfile.jdk$v
  docker build --pull -t "$CANON_IMAGE_NAME" . -f Dockerfile.jdk$v
  rm Dockerfile.jdk$v

  IMAGE_NAME="$CANON_IMAGE_NAME"
  if [ -n "${CI_DEPLOY:-}" ]; then
    if [ -n "${CI_COMMIT_TAG:-}" ]; then
      IMAGE_NAME="$IMAGE_NAME:$CI_COMMIT_TAG"
    else
      if [ -n "${CI_COMMIT_BRANCH:-}" ]; then
        IMAGE_NAME="$IMAGE_NAME/$CI_COMMIT_BRANCH:$CI_COMMIT_SHA"
      else
        IMAGE_NAME="$IMAGE_NAME/mr:$CI_COMMIT_SHA"
      fi
    fi
  fi

  docker tag $CANON_IMAGE_NAME "$CI_REGISTRY/$DOCKER_REGISTRY_PATH/$IMAGE_NAME"

  if [ -n "${CI_DEPLOY:-}" ]; then
    if [ "${CI_COMMIT_BRANCH:-}" == "master" -o -n "${CI_COMMIT_TAG:-}" ]; then
      docker push "$CI_REGISTRY/$DOCKER_REGISTRY_PATH/$IMAGE_NAME"
      if [ "${CI_COMMIT_BRANCH:-}" == "master" ]; then
        docker tag $CANON_IMAGE_NAME "$CI_REGISTRY/$DOCKER_REGISTRY_PATH/$CANON_IMAGE_NAME:latest"
        docker push "$CI_REGISTRY/$DOCKER_REGISTRY_PATH/$CANON_IMAGE_NAME:latest"
      fi
    fi
  fi
done
